/*
Metoda sterowanego przeszukiwania losowego CRS2. Wykonywane są kolejno
operacje losowania i przekształcania wielościanu utworzonego z punktów
wylosowanych z obszaru poszukiwań (odbicie). Zrównoleglenie - podział dziedziny
(uruchomienie dla kilku populacji punktów). Asynchroniczna wymiana najlepszych
aktualnie rozwiązań - co założoną liczbę iteracji. Wykonać testy dla funkcji AC
(dla n=2, 10, 20, 50, 100). Ocenić przyspieszenie obliczeń w zależności od
stopnia zrównoleglenia.
*/
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <stdbool.h>

#define PRINTABLE
#ifndef M_PI
#define M_PI           3.14159265358979323846
#endif

//ograniczenia
#define MIN_X -30.0f
#define MAX_X 30.0f
#define EPSILON 0.0000002f

// x struct
double getRandomX()
{
    return ( rand() / ((double)RAND_MAX+1)) * (MAX_X-MIN_X+1) + MIN_X;
}
typedef struct
{
    int id;
    int n;
    double* x_i;

    bool computed;
    double result;
} my_x;
void initOneX(my_x *new_x,int n, int id)
{
    new_x->id = id;
    new_x->n = n;
    new_x->x_i = calloc(n, sizeof(double));
    for(int i=0; i<n; i++)
        new_x->x_i[i] = getRandomX();

    new_x->computed = false;
}

void printX(my_x *x)
{
#ifdef PRINTABLE
    if(x->n > 5)
    {
        printf("dim. too big-> wont print this!\n");
        return;
    }
#endif
    printf("my_x %d: ", x->id);
    for(int i=0; i<x->n; i++)
        printf("\t%f", x->x_i[i]);
    if(x->computed)
        printf("\t[AC(X)]: \t%f", x->result);
    printf("\n");
}
void resetX(my_x *x)
{
    for(int i =0; i< x->n; i++)
        x->x_i[i] = 0;
}

// x vector struct
int getXCount(int n, int additional_count)
{
    if(additional_count < 1)
    {
        additional_count = 1;
        printf("[WARN]: additional_count should be grater than 1!");
    }
    return 10*(n+1) + additional_count;
}
typedef struct
{
    int count;
    my_x* x_vect;

    int min_index;
    int max_index;
} my_x_vect;
void initXVect(my_x_vect* new_vect, int n, int additional)
{
    int count = getXCount(n,additional);
    new_vect->count = count;

    my_x x;
    initOneX(&x, n, 1);
    new_vect->x_vect = calloc(count ,sizeof (my_x));
    if(!new_vect->x_vect)
        printf("\n<initXVect: err>\n");
    for(int i =0; i<count; i++)
    {
        initOneX(&new_vect->x_vect[i], n, i);
    }
}
void printXVect(my_x_vect *vect)
{
#ifdef PRINTABLE
    if(vect->x_vect[0].n > 5)
    {
        printf("dim. too big-> wont print this!\n");
        return;
    }
#endif
    printf("vector:\n");
    for(int i=0; i<vect->count; i++)
    {
        printf("\t");
        printX(&vect->x_vect[i]);
    }
}

void clearVect(my_x_vect *vect)
{
    for(int i=0; i<vect->count; i++)
        free(vect->x_vect[i].x_i);
    free(vect->x_vect);
    vect->x_vect = NULL;
}


// computing AC
double sum_2(my_x *x)
{
    double sum = 0;
    for(int i=0; i<x->n; i++)
        sum += pow(x->x_i[i],2);
    return sum;
}
double sum_cos(my_x *x)
{
    double sum = 0;
    for(int i=0; i<x->n; i++)
        sum += cos(2 * M_PI * x->x_i[i] );
    return sum;
}
double AC(my_x *x)
{
    double ac = 0;
    ac = - 20 * exp(-0.2 * sqrt(sum_2(x)/x->n))
         - exp(sum_cos(x)/x->n)
         + 20
         + exp(1); //this was missing in materials

    return ac;
}
void computeX(my_x * x)
{
    if(x->computed)
        return;
    x->result = AC(x);
    x->computed = true;
}

double getMin(my_x_vect * vect)
{
    return vect->x_vect[vect->min_index].result;
}
double getMax(my_x_vect * vect)
{
    return vect->x_vect[vect->max_index].result;
}
void computeVectX(my_x_vect * vect)
{
    vect->max_index = 0;
    vect->min_index = 0;

    computeX(&vect->x_vect[0]);
    for(int i=1; i<vect->count; i++)
    {
        computeX(&vect->x_vect[i]);
        if(vect->x_vect[i].result > getMax(vect))
            vect->max_index = i;
        else if(vect->x_vect[i].result < getMin(vect))
            vect->min_index = i;
    }
}

void printMinMax(my_x_vect * vect)
{
    printf("min idx: %d, min value: %f for\n\t",vect->min_index, getMin(vect));
    printX(&vect->x_vect[vect->min_index]);
    printf("max idx: %d, max value: %f for\n\t",vect->max_index, getMax(vect));
    printX(&vect->x_vect[vect->max_index]);
}

typedef struct
{
    int* indexes_to_get;
    int count;
} syplex;

void initSyplex(syplex* newSyplex, my_x_vect* vect, int n)
{
    // names ar a bit messy- n (dimensions count) is not related to in and rn
    // got from :
    // http://stackoverflow.com/questions/1608181/unique-random-numbers-in-an-integer-array-in-the-c-programming-language

    assert(vect->count >= getXCount(n,1));
    newSyplex->count = n+1;
    newSyplex->indexes_to_get = calloc((n+1) , sizeof(int));
    if(!newSyplex->indexes_to_get)
        printf("\n<initSyplex>: err\n");
    newSyplex->indexes_to_get[0] = vect->min_index;
    // the Knuth algorithm
    int in, im;
    im = 0;
    for(in = 0; in<vect->count - 1 && im < n ; ++in)
    {
        int rn = vect->count - 1 - in;
        int rm = n - im;
        if(rand() % rn < rm)
        {
            newSyplex->indexes_to_get[im+1] = (in+1>= vect->min_index ? in+1:in);
            im++;
        }
    }
    //printf("im: %d and count: %d\n", im,n);
    assert(im == n);
}
void deleteSyplex(syplex* s)
{
    free(s->indexes_to_get);
    s->indexes_to_get = NULL;
}
void printSyplex(syplex* s, my_x_vect* vect)
{
    printf("syplex: \n");
    for(int i=0; i<s->count; i++)
    {
//        int value_id = vect->x_vect[s->indexes_to_get[i] ].id;
//        double value_val = vect->x_vect[s->indexes_to_get[i] ].result;
//        printf("\n\t[%d]: AS(X[%d])=%f",i, value_id, value_val);
        printX(&vect->x_vect[s->indexes_to_get[i]]);
    }
    printf("\n");
}

bool checkEpsilon(my_x_vect* vect)
{
    return (fabs(getMax(vect) - getMin(vect))) <= EPSILON;
}
my_x getMiddleSyplex(syplex* s,my_x_vect *vect)
{
    my_x ret;
    ret.id = 1;
    ret.n = s->count-1;
    ret.x_i = calloc(ret.n , sizeof(double));
    if(!ret.x_i)
        printf("\n<getMiddleSyplex>: err\n");
    for(int i=0; i<ret.n; i++)
    {
        double sum = 0;
        for(int j =0; j<s->count-1; j++)
        {
            sum+= vect->x_vect[s->indexes_to_get[j]].x_i[i];
        }
        ret.x_i[i] = sum/(s->count -1);
    }
    return ret;
}
my_x reflex(syplex* s, my_x_vect* vect)
{
    my_x middle = getMiddleSyplex(s, vect);
    my_x x_to_reflect = vect->x_vect[s->indexes_to_get[s->count-1]];
//    printf("x count: %d and scount %d\n", x_to_reflect.n, s->count);
    my_x ret;
    ret.id =1;
    ret.n = s->count-1;
    ret.x_i = calloc(ret.n , sizeof(double));
    if(!ret.x_i)
        printf("\n<reflex>: err\n");

    ret.computed = false;

    for(int i = 0; i< ret.n; i++)
    {
        double greaterx = middle.x_i[i] ;
        double lesserx =  x_to_reflect.x_i[i];
        ret.x_i[i] = 2*greaterx-lesserx;
//        ret.x_i[i] = (2*middle.x_i[i] - x_to_reflect.x_i[i]);
    }
    free(middle.x_i);
    return ret;
}
bool greater(my_x* l, my_x* r)
{
    if(l->result > r->result)
        return true;
    return false;
}

bool check_bounds(my_x* x)
{
    for(int i=0; i<x->n; i++)
    {
        if(x->x_i[i] < MIN_X || x->x_i[i] > MAX_X)
            return false;
    }
    return true;
}
bool switchNewX(syplex* s, my_x_vect* vect)
{
    my_x xr = reflex(s,vect);
//    printX(&xr);
    if(check_bounds(&xr))
    {
        computeX(&xr);
//        printf("computed xr: %f for ", xr.result);
        if(greater(&xr, &(vect->x_vect[vect->max_index])))
        {
            free(xr.x_i);
            return false;
        }else
        {
//            printf("changing %d to new\n", vect->max_index);
            xr.id = vect->max_index;
            free(vect->x_vect[vect->max_index].x_i);
            vect->x_vect[vect->max_index] = xr;
            computeVectX(vect);
            return true;
        }
    }else{
//        printf("bad bounds\n");
        free(xr.x_i);
        return false;
    }
}
void printCheckDistribution(my_x_vect* vect)
{
    int *a;
    int count = (MAX_X - MIN_X)/10;
    int count2=0;
    a = calloc(count, sizeof(int));

    for(int i = 0; i< vect->count; i++)
    {
        for(int j = 0; j< vect->x_vect[i].n; j++)
        {
            count2++;
            int k=1;
            bool c = true;
            while(c)
            {
                if(vect->x_vect[i].x_i[j] < (k*10 +MIN_X))
                {
                    c = false;
                    a[k-1]++;
                }
                k++;
            }
        }
    }
    printf("distribution:\n");
    for(int i =0; i< count; i++)
    {
        printf("\t< %d ;\t%d >: \t%d / %d\t(%d %%)\n", (int)(i*10 + MIN_X), (int)((i+1)*10 + MIN_X), a[i],count2, a[i]*100/count2);
    }
}

int main(void)
{
    srand(time(NULL));
///////////////
///  checking x
///////////////
//    my_x a;
//    int n =2; int id=1;
//    initOneX(&a,n,id);
//    printX(&a);

///////////////
///  checking vect
///////////////
//    my_x_vect a;
//    initXVect(&a, 2,1);
//    printXVect(&a);

///////////////
///  checking AC
///////////////
//    my_x a,b,c,d,e,f;
//    int n=3;
//    initOneX(&a,n,1);
//    initOneX(&b,n,1);
//    initOneX(&c,n,1);
//    initOneX(&d,n,1);
//    initOneX(&e,n,1);
//    initOneX(&f,n,1);
//    printX(&a);
//    double first_result = AC(&a);
//    printf("a result = %f\n", first_result);
//    printX(&b);
//     first_result = AC(&b);
//    printf("b result = %f\n", first_result);
//    printX(&c);
//     first_result = AC(&c);
//    printf("c result = %f\n", first_result);
//    printX(&d);
//     first_result = AC(&d);
//    printf("d result = %f\n", first_result);
//    printX(&e);
//     first_result = AC(&e);
//    printf("e result = %f\n", first_result);
//    printX(&f);
//    first_result = AC(&f);
//    printf("f result = %f\n", first_result);

///////////////
/// checking computeVectX
///////////////
//    my_x_vect a;
//    initXVect(&a,2,1);
//    printXVect(&a);

//    computeVectX(&a);
//    printMinMax(&a);

///////////////
/// checking syplex
///////////////
//    int n = 2;
//    my_x_vect a;
//    initXVect(&a, n, 1);
//    computeVectX(&a);

//    syplex syp;
//    initSyplex(&syp, &a, n);
//    printSyplex(&syp, &a);
///////////////
/// checking syplex (for segfault)
///////////////
//while(true){
//    printf("\n\n\n\n");
//    int n = 1;
//    my_x_vect a;
//    initXVect(&a, n, 1);
//    computeVectX(&a);
//    printXVect(&a);
//    computeVectX(&a);

//    syplex syp;
//    initSyplex(&syp, &a, n);
//    printSyplex(&syp, &a);

//    switchNewX(&syp, &a);
//    printXVect(&a);
//}
///////////////
/// checking algo
///////////////
    printf("====================================\n");
    printf("starting\n");
    printf("====================================\n");
    int n = 100; // dimension of x
    my_x_vect a;

    initXVect(&a, n, 1);
    bool new_better = false;
    computeVectX(&a);
    printf("starting vector of X (%d dimesnion)\n", n);
    printXVect(&a);
    printf("====================================\n");
    printCheckDistribution(&a);
    printf("====================================\n");
    while(true)
    {
        if(new_better)
            computeVectX(&a);
        syplex syp;
        initSyplex(&syp, &a, n);
        //printSyplex(&syp,&a);

        if(switchNewX(&syp, &a)){
            new_better = true;
//            printf("found new\n");
        }else
            new_better = false;
        deleteSyplex(&syp);
        if(checkEpsilon(&a))
        {
            printf("\rbest result: %f, worst result: %f", getMin(&a), getMax(&a));
            printf("\n====================================\n");
            printf("\rDONE:\n");
            printf("\t");
            printX(&a.x_vect[a.min_index]);
//            printXVect(&a);
//            printf("best: %f, worst: %f\n", getMin(&a), getMax(&a));
            return 0;
        }else{
//            printf("AC(X[%d] = %f)\n", a.max_index, getMax(&a));
        }
        printf("\rbest result: %f, worst result: %f", getMin(&a), getMax(&a));
    }
    clearVect(&a);

    return 0;
}

